# ArmoredPipe

## KMS

### DB_Tables

#### Auth
| Username | Password  | Certificato | SSK | TTL_SSK | ID | FakeIds|CanRead|Canwrite|PublicKey
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| Stringa |Hash  |File |Chiave |Time|Int/Srting |Vettore[Int]|Vettore[String]|Vettore[String]|Chiave|

#### Simmetric_Keys
| SMDK|TimeSpamp_Generation|ID|Topic|TTL_SMDK|TTL_SDK|Counter
|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
|Chiave|Time|Int/String|String|Time|Time|Int|

### Variabili
- ID;
- PrivateKey;
- PublicKey;

### Pseudo codice

- **masterPTP();**
	Applica il protocollo PTP sincronizzando il client a se stesso utilizzando la funzione _slavePTP();_ presente sul client.

___
- **authUser(username, password, certificato);**
	Controlla che username, password e certificato siano validi.
___
- **genKey(lunghezza);**
	Genera una chiave.
___
- **genNonce();**
	Genera una stringa pseudocasuali.
___
- **genId(username);**
	Genera un id dato un _username_.
___
- **encryptionRSA(publicKey, dato, NONCE);**
	(assimetrico) cripta il _dato_ utilizzando la _publicKey_  mischiando con il _NONCE_.
___
-  **decryptionRSA(privateKey, dato, NONCE);**
	(assimmetrico) decripta il _dato_ utilizzando la _privateKey_.
___
- **encryptionAES(simmetricKey, dato);**
	(simmetrico) cripta il _dato_ utilizzando la chiave simmetrica.
___
- **decryptionAES(simmetricKey, dato)**
	(simmetrico) decripta il _dato_ utilizzando la chiave simmetrica.
___
- **canWrite(ID, topic);**
	Controlla se un s con id = _ID_ può scrivere su un certo _topic_.
___
- **canRead(ID , topic)**
	Controlla se un s con id = _ID_ può leggere su un certo _topic_.
___
- **getSSK(id);**
	Restituisce la chiave SSK attuale associato al'_id_.
___
- **genSequence();**
	Genera un sqInfo contenete un numero di sequenza inziale e un numero per incrementare il seq.
___
- **getID();**
	Restituisce l'ID.
___
- **pub(topic, msg,  id);**
	Pubblica un messaggio(_msg_) su un determinato _Topic_ mettendo esplicitamente l'_id_ .
___
- **getUsername(id)**
	Restituisce l'username associato al'_id_.
___
-**setID(oldID, newID);**
	Imposta l'id di S con id = _oldID_ a _newID_.
___
- **getSMDK(topic, data_inizio, data_fine)**
	Restituisce la chiave SMDK, l'id, il TTL e il counter utilizzato su un _topic_ tra la _data_inizio_ e la _data_fine_.
___
- **getPublicKey();**
	Restituisce la public key.
___
- **getPrivateKey();**
	Restituisce la chiave privata.
___
- **listenID(topicList, ID);**
	Sta in ascolto sui topic specificati nella _topicList_ e intercetta quelli con id = _ID_
___
- **challangeSSK(key, NONCE, msg);**
	Decripta il _msg_ con la chive (_key_) e confronta il risultato con il _NONCE_
___
- **updateSSKKey(key, TTL, ID);**
	Aggiorna la chiave SSK del utente con id = _ID_ con la chiave _key_ e il _TTL_.-
___
- **genFakeIdvector();**
	Genera un vettore di fake id.
___
- **getFakeIdFor(ID)**
	Restituisce un fakeId per comunicare con l'S con l'id = _ID_ 
___
- **listen();**
	Intercetta tutti i messaggi sui tre topic di supporto writeTopic, readTopic e loginTopic.
	Dopo di che in base al id presente nel messaggio e alla struttura dello stesso (se il msg non è criptato 	sarà sicuramente un S_HELLO ) richiama la funzione adatta a gestirlo.
	Se si tratta di una procedura con un numero maggiore di scambi di messaggi allora i successivi messaggi con id gia visto verrano gestiti all'interno dell'apposita funzione e quindi non dovranno essere 	prese in carico da questo metodo (funzione di lock dell'ID), in piu i messaggi con id associato a quello 	del DB non devono mai essere presi in cosiderazione 
	
	- IF(msg=loginTopic&&"non criptato") THEN(login(msg, id));
	- IF(msg=loginTopic&&"criptaro")THEN(loginSSK(msg, id));
	- IF(msg=loginTopic&&getUsername(id)="fakeId")THEN(NULL);
	- IF(msg=writeTopic&&getUsername(id)!="fakeId)THEN(write(msg, id))ELSE_IF(getUsername(id)=="DB")THEN(counterACK(msg,ID)));
	- IF(msg=readTopic&&getUsername(id)!="DB"&&"fakeID")THEN(read(msg,ID));
___
- **login(msg, id);**

	1. *masterPTP();*

	2. *genNonce();*
	
	3. *pub(loginTopic, msg, Id);*
		
		_msg_: Richiesta certificato, username, psw, e _NONCE_(putno 2) 
		_id_: (parametro della funzione)
	
	4. *lstenID(loginTopic, id );*
		
		_id_:(parametro della funziona)
		
	5. *decryptionRSA(getPrivateKey(), msg, NONCE)* 
		
		_pkg_: (punto 4)
		_NONCE_ (punto 2)
	
	6. *authUser(username, password, certificato);*
		
		Controlla i dati decriptati al punto 5
		In caso non siano riconosciuti manda un messaggio a S

	7. *genKey(lunghezza);*
	8. *genId(username);*
	9. *genFakeIdVec();*
	10. *encryptionRSA(PK_S, dati, NONCE);*
		
		_dati_: SSK, TTL(punto 7) e ID(punto 8)
		_NONCE_: (punto 2).

	11. *pub(loginTopic, msg, id)*
		
		_msg_: (punto 10)
		_id_  (parametro della funzione)
___
- **loginSSK(msg, id);**

	1. *getSSK(id)*
		
		_id_: (parametro funzione)

	2. *genKey();*
	3. *genId(getUsername(id));*
	4. *genNonce();*
	5. *genFakeIdVec();*
	6. *encryptionAES(SSK, dati)*
		
		_SSK_: (punto 2)
		_dati_:  chiave SSK e TTL (punto 2), l'_id_ (punto 3), il fakeIdVector (punto 5) e il NONCE (punto 4)

	7. *pub(loginTopic, msg, getFakeId(ID));*
		
		_msg_: (punto 6) 
		_ID_ : (parametro funzione)

	8. *listenID(topicLogin, id);*
		
		_id_:(parametro funzione)

	9. *challengeSSK(key, NONCE, msg);*
		
		_key_:(punto 2)
		_NONCE_:(punto 4)
		_msg_:(punto 8)
		in caso contrario lascia quella attuale e invia un messaggio a S

	10. *updateID(getID(DB), newID)*
		
		_newID_: (punto 3)

	11. updateSSKKey(key, TTL, ID)
		
		_key, TTL_: (putno 2)
		_ID_: (punto 3)
___
- **write(msg, id);**

	1. getSSK(id);
		
		_id_: (parametro funzione)

	2. *decryptionAES(SSK, msg);*
		
		_SSK_: (punto )
		_msg_: (parametro funzione)

	3. *canWrite(id, topic);*
		
		_id_: (parametro funzione)
		_topic_: (punto 2)
		in caso l'user della richiesta non sia autorizzato a scrivere sul topic invia un messaggio a S

	4. *genKey();* 
		
		SMDK.

	5. *genKey();* 
		
		SDBK

	6. *getID ("DB");*
	7. *genSequence();*
	8. *encryptionAES(getSSK(getID(DB)), dati);*
		
		_dati_: chiave e TTL (punto 4) id di S (parametro funzione) sqInfo (punto 6)

	9. *pub(writeTopic, msg, getFakeID(getI(DB));*
		
		_msg_: (punto 7)

	10. *encryptionAES(getSSK(id), dati);*
		
		_dati_: chiavi, TTL (punto 3,4) e sqInfo (punto 6) 

	11. *pub(writeTopic, msg, getFakeId(id));*
		
		_msg_: (punto 8)
		_id_: (parametro funzione)
___
- **countenrACK(msg, id);**

	1. *decryptionAES(getSSK(id), msg);*  
		
		_msg, id_ : (parametro funzione)	
		
	2. Incrementa il contatore della chiave con i parametri presenti nel punto 1.
___
- **read(msg, id);**

	1. *decryptionAES(getSSK(id), msg );*
	2. *canRead(getUsername(id), topic);*
		
		_topic_: (punto 1)
		in caso contrario invia un messaggio a S

	3. *genKey();*
		
		SDBK

	4. *getSMDK(topic, data_inizio, data_fine);*
		
		_topic, data_inizio, data_fine_:_msg_(parametro funzione)

	5. *encryptionAES(SSK, dati);*
		
		_SSK_:(punto 2)
		_dati_: SDBK, SMDK, TTLs, counter, owner(punto 3,4) id del DB.
		Le chiavi e i vari dati restituiti al punto 4 potrebbero essere multipli.

	6. *pub(writeTopic, msg, getFakeId(id));*
		
		_msg_:(punto 5)
		_id_: (parametro funzione)

	7. *encryption(getSSK(getID(DB)), dati);*
		
		_dati_: topic, data_inizio, data_fine (punto 1) e  la chiave SDBK (punto 3)

	8. *pub(writeTopic, msg, getFakeId(getId(DB)));*
		
		_msg_:  (punto 7)
___
## DB
### DB_Tables
#### Data
|       Dato       | TimestampCryp |  ID_Owner  | MD5  | Topic  |
| :-: | :-: | :-: | :-: | :-: |
| Stringa Criptata |     Time      | int/String | MD5  | String |

Nella versione finale un DB per topic quindi non ci sarà la colonna "Topic"

### Variabili
- ID;
- Certificato;
- Auth;
- SSK;
- fakeIdVector;

###  Pseudo codice
- **slavePTP();**
	Insieme a *masterPTP();* applica il protocollo PTP.
___
- **pub(topic, msg,  id);**
	 Pubblica un messaggio(_msg_) su un determinato _Topic_ mettendo esplicitamente l'_id_.
___
- **encryptionRSA(publicKey, dato, NONCE);**
	(assimetrico) cripta il _dato_ utilizzando la chiave pubblica  mischiando con il _NONCE_.
___
- **decryptionRSA(privateKey, dato, NONCE);**
	(assimmetrico) decripta il _dato_ utilizzando la chiave privata.
___
- **encryptionAES(simmetricKey, dato);**
	(simmetrico) cripta il _dato_ utilizzando la chiave simmetrica.
___
- **decryptionAES(simmetricKey, dato)**
	(simmetrico) decripta il _dato_ utilizzando la chiave simmetrica.
___
- **getId();**
	Restituisce l'ID.
___
- **getCertificate();**
	Restituisce il certificato di validità del DB.
___
- **getAuth();**
	Restituisce l'username e la psw del DB.
___
- **setID(id);**
	Setta l'id del DB con l' _id_.
___
- **setSSK(sskKey, ttlKey);**
	Imposta la chiave ssk = _sskKey_ e il TTL= _ttlKey_.
___
- **listenID(topicList, S_ID);**// da rivedere
	Sta in ascolto sui topic elencati in _topicList_ e intercetta i messaggi con id = _S_ID_ per poi richiamare la funzione adeguato per la gestione del messaggio.
___
- **getSSK_KMS();**
	Restituisce la chiave SSK utilizata per comunicare con il KMS
___
- **getData(topic, data_inizio, data_fine);**
	Restituisce i dati presenti nel DB pubblicati su un _topic_ tra la _data_inizio_ e la _data_fine_ insiame al correspittvo id.
___
- **validMD5(MD5, pkg);**
	Controlla se il codice _MD5_ corrisponde al pachetto _pkg_.
___
- **genMD5(pkg)**
	Calcola e restituisce il file MD5 di un _pkg_.
___
- **validSequence(sq, sqInfo);**
	Controlla che l'_sq_ rispetti i parametri del _sqInfo_ e che non sia gia stato ricevuto (controllo replay attack).
___
- **storeData(dato, timestampEncryp, topic, ID );**
	Salva nel db i parametri della funzione .
___
- **genTempID();**
	Genera un id temporaneo che usare il kms come alias per cominicaro con S.
___
- **setFakeIdVector(fakeIdVec);**
	Imposta il fakeIdVector = _fakeIdVec_.
___
- **getFakeIdVector();**
	Restituisce lista degli alias usati dal KMS.
___
- **login();**
	1. *genTempID();*
	2. *pub(loginTopic, msg, getId());*
		
		_msg_:  tempID (punto 1)

	3. *listenID(loginTopic, id );*
		
		_id_: (punto 1)

	4. *getAuth();*
	5. *getCertificate();*
	6. *encryptionRSA(PK_KMS, dato, NONCE);*
		
		_PK_KMS_: _msg_ (punto 3)
		_dato_: username, password (punto 4) e certificato (punto 5)
		_NONCE_: _msg_(punto 3)

	7. *pub(writeTopic, msg, getId());*
		
		_msg_: _dato_ (punto 3)

	8.  *listenID(loginTopic, id );*
		
		_id_: (punto 1)

	9.  *decryptionRSA(PrivateKey_S, msg, NONCE);*
		
		_msg_: (punto 8)
		_NONCE_: (punto 3)

	10. *setID(id);*
		
		_id_: (punto 9)

	11. *setFakeIdVector(fakeIdVec);*
		
		_fakeIdVec_ : (punto 9)
		
	12.  *setSSK(sskKey, ttlKey);*
		
		_sskKey, ttlKey_: (punto 9) 
___
- **loginSSK();**
	1. *encryptionAES(getSSK_KMS(), dati);*
		
		_dati_: richiesta rinnovo chiave e id

	2. *pub(loginTopic, msg, id);*
		
		_msg_: (punto 1)

	3. *listenID(loginTopic, getFakeIdVector());*
	4. *decryptionAES(SSK, dati);*
		
		_dati_: (punto 3)

	5. *encryptionAES(SSK, NONCE);*
		
		_SSK, NONCE_: (punto 4)

	6. *pub(loginTopic, msg, getId);*
		
		_msg_: (punto 5)
	
	6. *setID(id);*
		
		_id_: (punto 4)

	7. *setFakeIdVector(fakeIdVec);*
		
		_fakeIdVec_ : (punto 4)

	8. *setSSK(sskKey, ttlKey);*
		
		_sskKey, ttlKey_: (punto 4) 
___
- **writeReq(msg)**
	1. *getSSK_KMS();*
	2. *decryptionAES(SSK_KMS, dati);*
		
		_SSK_KMS_: (punto 1)
		_dati_: _msg_(paramentro funzione)

	3. *listenID(topic, id);*
		
		_id_: (punto 2)
		_topic_: (punto 2)
		il messaggio sarà composto da body e head 

	4. *decryptionAES(SDBK, dati)*
		
		_SDBK_: (punto 2)
		_msg_: head (punto 3)

	5. *validSequence(sq, sqInfo);*
		
		_sq_: (punto 4)
		_sqInfo_: (punto 2)

	6. *validMD5(MD5, pkg);*
		
		_MD5_: (punto 4)
		_pkg_: body (punto3)

	7. *decryptionAES(SDBK, dati)*
		
		_SMDK_: (punto 2)
		_msg_: body (punto 3)

	8. *storeData(dato, timestampEncryp, topic, id );*
		
		_dato, timestampEncryp, topic, id_: (punto 7)
		
	9. *encryptionAES(getSSK_KMS(), dati);*
		
		_dati_: id_s e topic (punto 2) e risultato operazione di store (ACK) 

	10. *pub(readTopic, msg, getId());*
		_msg_:(punto 9)
___
- **readReq(msg);**
	1. *getSSK_KMS();*
	2. *decryptionAES(SSK_KMS, dati);*
		
		_SSK_KMS_: (punto 1)
		_dati_: _msg_(paramentro funzione)
	
	3. *getData(topic, dataInizio, dataFine);*
		
		_topic, dataInizio, dataFine_: (punto 2)

	4. *encryptionAES(SDBK, dati);*
		
		_dati_: (punto 3)
		_SDBK_: (punto 2)  
		SDBK_BODY

	5. *encryptionAES(SDBK, genMD5(pkg));*
		
		_pkg_: 	(punto 3)
		_SDBK_: (punto 2)
		SDBK_HEAD

	6. *pub(writeTopic, msg, getId());*
		
		_msg_: (punto 4,5)	
___
## S
### Variabili
- ID;
- Auth;
- Certificate (Contente PK_KMS);
- SSK;
- fakeIdVector;

### Pseudo codice
- **slavePTP();**
	Insieme a *masterPTP();* applica il protocollo PTP.
___
- **pub(topic, msg,  id);**
	Pubblica un messaggio(_msg_) su un determinato _Topic_ mettendo esplicitamente l'_id_.
___
- **encryptionRSA(publicKey, dato, NONCE);**
	(assimetrico) cripta il _dato_ utilizzando la chiave pubblica  mischiando con il _NONCE_.
___
- **decryptionRSA(privateKey, dato, NONCE);**
	(assimmetrico) decripta il _dato_ utilizzando la chiave privata.
___
- **encryptionAES(simmetricKey, dato);**
	(simmetrico) cripta il _dato_ utilizzando la chiave simmetrica.
___
- **decryptionAES(simmetricKey, dato)**
	(simmetrico) decripta il _dato_ utilizzando la chiave simmetrica.
___
- **getId();**
	Restituisce l'attuale id.
___
- **getCertificate();**
	Restituisce il certificato di validità del DB.
___
- **getAuth();**
	Restituisce l'username e la psw del DB.
___
- **setID(id);**
	Setta l'id del DB con l' _id_.
___
- **setSSK(sskKey, ttlKey);**
	Imposta la chiave ssk = _sskKey_ e il TTL= _ttlKey_.
___
- **setSDBK(sdbkKey, ttlKey);**
	Imposta la chiave ssk = _sdbkKey_ e il TTL= _ttlKey_
___
- **setSMBK(smdkKey, ttlKey, topic);**
	Imposta la chiave ssk = _smdkKey_ ,il TTL= _ttlKey _ e specifica il _topic_.
___
- **listenID(topicList, S_ID);**
	Sta in ascolto sui topic elencati in _topicList_ e intercetta i messaggi con id = _S_ID_ 
___
- **getSSK_KMS();**
	Restituisce la chiave SSK utilizata per comunicare con il KMS.
___
- **validMD5(MD5, pkg);**
	Contrlla se il codice _MD5_ corrisponde al pachetto _pkg_.
___
- **genMD5(pkg)**
	Calcola e restituisce il file MD5 di un _pkg_.
___
- **genTempID();**
	Genera un id temporaneo che usare il kms come alias per cominicaro con S.
___
- **setFakeIdVector(fakeIdVec);**
	Imposta il fakeIdVector = _fakeIdVec_.
___
- **getFakeIdVector();**
	Restituisce lista degli alias usati dal KMS.
___
- **expandSMDK(SMDK, n);**
	Espande la chiave _SMDK_ _n_ volte.
___
- **validTTL(key)**
	Restituisce true se il ttl della key è ancora valido.
___
- **getSDK(SMDK, tistampGen, timestampCryp, TTL_SDK  );**
	1.  $$ \Delta = timestampCryp - timestampGen$$
	2.  $$N = \frac {\Delta - (\Delta \mod TTL_{SDK})}{TTL_{SDK}} $$
	3.  return *expandSMDK(SMDK, N);*
		
		_SMDK_:(parametro funzione)
		_N_: (punto 2)
___
- **login();**
	1. *genTempID();*
	2. *pub(loginTopic, msg, getId());*
		
		_msg_:  tempID (punto 1)
		
	3. *listenID(loginTopic, id );*
		
		_id_: (punto 1)

	4. *getAuth();*
	5. *getCertificate();*
	6. *encryptionRSA(PK_KMS, dato, NONCE);*
		
		_PK_KMS_:  (punto 5)
		_dato_: username, password (punto 4) e certificato (punto 5)
		_NONCE_: _msg_(punto 3)

	7. *pub(writeTopic, msg, getId());*
		
		_msg_: _dato_ (punto 3)

	8.  *listenID(loginTopic, id );*
		
		_id_: (punto 1)

	9.  *decryptionRSA(PrivateKey_S, msg, NONCE);*
		
		_msg_: (punto 8)
		_NONCE_: (punto 3)
		
	10. *setID(id);*
		
		_id_: (punto 9)

	11. *setFakeIdVector(fakeIdVec);*
		
		_fakeIdVec_ : (punto 9)

	12. *setSSK(sskKey, ttlKey);*
		
		_sskKey, ttlKey_: (punto 9) 
___
- **loginSSK();**
	1. *encryptionAES(getSSK_KMS(), dati);*
		
		_dati_: richiesta rinnovo chiave e id

	2.  *pub(loginTopic, msg, id);*
		
		_msg_: (punto 1)

	3. *listenID(loginTopic, getFakeIdVector());*
	4. *decryptionAES(SSK, dati)*
		
		_dati_: (punto3)

	5. *encryptionAES(SSK, NONCE);*
		
		_SSK, NONCE_: (punto 4)

	6. *pub(loginTopic, msg, id);*
		
		_msg_: (punto 5)

	6. *setID(id);*
		
		_id_: (punto 4)

	7. *setFakeIdVector(fakeIdVec);*
		
		_fakeIdVec_ : (punto 4)

	8. *setSSK(sskKey, ttlKey);*
		
		_sskKey, ttlKey_: (punto 4) 
___
- **write(topic, dato);**
	1. *encrytptionAES(getSSK_KMS(), msg);*
		
		_msg_: _topic_(parametro funzione)

	2. *pub(writeTopic, msg, getId);*
		
		_msg_: (punto 1)

	3. *listenID(writeTopic, getFakeIdVector());*
	
	4. *decryptionAES(getSSK_KMS, msg)*
		
		_msg_: (punto 3)
	
	5. *setSDBK(sdbkKey, ttlKey);*
		
		_sdbkKey, ttlKey_: (punto 4)
	
	6. *setSMDK(smdkKey, ttlKey, topic);*
		
		_smdkKey, ttlKey, topic_: (punto 4)
		
	7. WHILE(*validTTL(key)*)
		
		_key_: _SDBK_ (punto 4)	
			
			a1. WHILE(*validTTL(SMDK)*)
				
				_SMDK_: (punto 4) 
					
					b1. expandSMDK(SMDK, n)
						
						_SMDK_: (punto 4) 
						_n_: intero che andra incrementato di volta in volta

					b2. WHILE(*validTTL(SDK)*)
						
						_SDK_: (punto b1)		
							
							c1. *encryptionAES(SDK, dati);*
								
								_SDK_: (punto b1)
								_dati_: (parametro funzione)
							
							c2. *encryptionAES(SDBK, dati);*
								
								_SDBK_: (punto 4)
								_dati_: (punto c1), timestamp del punto c1 
								SDBK_BODY
							
							c3. *genMD5(pkg);*
								
								_pkg_: (punto c2)
							
							c4. *encryptionAES(SDBK, dati)*
								
								_dati_: (punto c3) e sq (punto 3)
								SDBK_HEAD
							
							c5. *pub(topic, msg);*
								
								_topic_: (paramentro funzione)
								_msg_: (punto c2) + (punto c4)
							
							c6. closeWhile "c"
					b3. closeWhile "b"
			a2. closeWhile "a"
___
- **read(topic, dataInizio, dataFine);**
	1. *encrytptionAES(getSSK_KMS(), msg);*
		
		_msg_: _topic, dataInizio, dataFine_(parametro funzione)

	2. *pub(writeTopic, msg, getId);*
		
		_msg_: (punto 1)

	3. *listenID(writeTopic, getFakeIdVector());*
	4. *decryptionAES(getSSK_KMS, msg)*
		
		_msg_: (punto 3)

	5. *listenID(writeTopic, id);*
		
		_id_: id del DB (punto 4)

	6. *decryptionAES(SDBK, msg)*
		
		_msg_: (punto 5)
		_SDBK_: (punto 4)
		SDBK_HEAD

	7. *validMD5(MD5, pkg)*
		
		_MD5_: (punto 6)
		_pkg_: (punto 5) 

	6. *decryptionAES(SDBK, msg)*
		
		_msg_: (punto 5)
		_SDBK_: (punto 4)
		SDBK_BODY

	7. getSDK(SMDK, timestampCryp, timestampGen, TTL_SDK)
		
		_SMDK, timestampCryp, timestampGen, TTL_SDK_ : (punto 4)

	8. *decryptionAES(SDK, dato);*
		
		_dato_: (punto 6)
		_SDK_: (punto 7)
___

## Schemi di sequenza

### Login

![](login.svg)

### LoginSSK

![](LoginSSK.svg)

### Write

![](Write.svg)

### Read

![](Read.svg)
